package flights.service;

import flights.dao.FlightDAO;
import flights.model.Flight;

import java.util.Date;
import java.util.List;

public class FlightService {
    private static FlightService flightService;
    private FlightDAO flightDAO;

    private FlightService() {
        flightDAO = FlightDAO.getInstance();
    }

    public static FlightService getInstance() {
        if (flightService == null) {
            flightService = new FlightService();
        }
        return flightService;
    }

    public String postFlight(String type, String departureCity, String arrivalCity, Date departureTime, Date arrivalTime) {
        return flightDAO.addFlight(type, departureCity, arrivalCity, departureTime, arrivalTime);
    }

    public String updateFlight(long id, String type, String departureCity, String arrivalCity, Date departureTime, Date arrivalTime) {
        return flightDAO.updateFlight(id, type, departureCity, arrivalCity, departureTime, arrivalTime);
    }

    public Flight getFlight(String flightNumber) {
        return flightDAO.getFlight(flightNumber);
    }

    public String deleteFlight(int number) {
        return flightDAO.deleteFlight(number);
    }

    public List getFlights() {
        return flightDAO.getFlights();
    }
}
