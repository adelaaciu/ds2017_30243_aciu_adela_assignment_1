package flights.service;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import flights.dao.CityDAO;
import flights.model.City;
import flights.model.GoogleTimezone;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.LocalDate;
public class CityService {

    private static CityService service;
    private CityDAO cityDAO;

    private CityService() {
        cityDAO = new CityDAO();
    }

    public static CityService getInstance() {
        if (service == null) {
            service = new CityService();
        }
        return service;
    }

    public City getCity(String name) {
        return cityDAO.getCity(name);
    }

    public City getCity(long id) {
        return cityDAO.getCity(id);
    }

    public Timestamp getLocalTime(City city, LocalDate date, Time time) {
        Timestamp localTime = new Timestamp(date.getYear(),
                date.getMonthValue(),
                date.getDayOfMonth(),
                time.getHours(),
                time.getMinutes(),
                time.getSeconds(),
                0);
        try {
            return getLocationTime(city, localTime);
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        return localTime;
    }


    public Timestamp getLocalTime(City city, Date date) {
        Timestamp localTime = new Timestamp(
                date.getYear(),
                date.getMonth(),
                date.getDay(),
                date.getHours(),
                date.getMinutes(),
                date.getSeconds(),
                0);
        try {
            return getLocationTime(city, localTime);
        } catch (Exception e) {
            System.out.print(e.getMessage());
        }
        return localTime;
    }

    private Timestamp getLocationTime(City city, Timestamp timestamp) throws Exception {
        String url = "https://maps.googleapis.com/maps/api/timezone/json?location=" + city.getLatitude() + "," + city.getLongitude() + "&timestamp=" + timestamp.getTime() / 100 + "&key=AIzaSyCwhlxDjLYNMNoIA3sKD9u5gb9RYjjYNWY";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        ObjectMapper mapper = new ObjectMapper();

        try {
            GoogleTimezone googleTimeZoneResponse = mapper.readValue(response.toString(), GoogleTimezone.class);
            int offsets = googleTimeZoneResponse.getDstOffset() * 1000 + googleTimeZoneResponse.getRawOffset() * 1000;
            return new Timestamp(timestamp.getTime() + offsets);
        } catch (JsonMappingException ex) {
            System.out.println(ex.getMessage());
        }
        return timestamp;
    }

    public void saveCity(String name, String latitude, String longitude) {
        City city = new City();
        city.setName(name);
        city.setLatitude(latitude);
        city.setLongitude(longitude);
        cityDAO.addCity(city);
    }
}
