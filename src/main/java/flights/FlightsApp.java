package flights;

import flights.model.User;
import flights.service.CityService;
import flights.service.LoginService;

public class FlightsApp {

    public static void main(String[] args) {
        LoginService loginService = new LoginService();
        createUser(loginService);

        CityService cityService = CityService.getInstance();
        cityService.saveCity("Cluj-Napoca","46.7667 ","23.6");
        cityService.saveCity("Dubai","25.276987","25.276987");
        cityService.saveCity("NewYork","40.730610","-73.935242");
    }

    private static void createUser(LoginService loginService) {
        User admin = loginService.getUserByUserName("admin");
        User user = loginService.getUserByUserName("user");
        if (admin == null)
            loginService.addUser("admin", "admin", "admin");
        if (user == null)
            loginService.addUser("user", "user", "user");
    }
}
