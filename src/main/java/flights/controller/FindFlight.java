package flights.controller;

import flights.model.Flight;
import flights.service.FlightService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class FindFlight extends HttpServlet {

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//http://localhost:8080/FindFlight?number=1

        response.setContentType("text/html");
        String number = request.getParameter("number");
        PrintWriter out = response.getWriter();

        FlightService service = FlightService.getInstance();
        Flight flight = service.getFlight(number);
        String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
        System.out.println(flight);
        if(flight != null){
            out.println(docType+
                    "<html>\n" +
                    "<head><title>" + flight.getId() + "</title></head>\n" +
                    "<body bgcolor = \"#f0f0f0\">\n" +
                    "<h1 align = \"center\">" + flight.getType() + "</h1>\n" +
                    "<ul>\n" +
                    "  <li><b>Departure City:</b>: "
                    + flight.getDepartureCity() + "\n" +
                    "  <li><b>Departure Date:</b>: "
                    +  flight.getDepartureTime() + "\n" +
                    "  <li><b>Arrival City : City:</b>: "
                    +  flight.getArrivalCity() + "\n" +
                    "  <li><b>Arrival  Date:</b>: "
                    +  flight.getArrivalTime() + "\n" +
                    "</ul>\n" +
                    "</body>"+
                   " </html>"


            );
        } else {
            out.println(docType +
                    "<html>\n" +
                    "<head><title>" + "Post flight" + "</title></head>\n" +
                    "<body bgcolor = \"#f0f0f0\">\n" +
                    "<h1 align = \"center\">" + "NOT_FOUND" + "</h1>\n" +
                    "</body> " +
                    "</html>"
            );
        }

    }
}
