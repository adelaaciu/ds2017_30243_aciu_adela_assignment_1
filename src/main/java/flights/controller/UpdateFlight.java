package flights.controller;

import flights.service.FlightService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UpdateFlight extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("flightNumber"));
        String airplaneType = request.getParameter("airplaneType");
        String departureCityName = request.getParameter("departureCityUpdate");
        String arivalCityName = request.getParameter("arrivalCityUpdate");
        String departureDate = request.getParameter("departureDate");
        String arivalDate = request.getParameter("arrivalDate");
        FlightService service = FlightService.getInstance();


        SimpleDateFormat oldFormat = new SimpleDateFormat("yyyy-mm-dd'T'hh:mm");
        SimpleDateFormat newFormat = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");

        Date departureTime = null;
        if(departureDate!= "") {
            try {
                departureDate = newFormat.format(oldFormat.parse(departureDate));
                departureTime = newFormat.parse(departureDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        Date arrivalTime = null;
        if(arivalDate!= "") {
            try {
                arivalDate = newFormat.format(oldFormat.parse(arivalDate));
                arrivalTime = newFormat.parse(arivalDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        String rsp = service.updateFlight(id, airplaneType, departureCityName, arivalCityName, departureTime, arrivalTime);

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";

        out.println(docType +
                "<html>\n" +
                "<head><title>" + "Post flight" + "</title></head>\n" +
                "<body bgcolor = \"#f0f0f0\">\n" +
                "<h1 align = \"center\">" + rsp + "</h1>\n" +
                "</body> " +
                "</html>"
        );
    }
}
