package flights.controller;

import flights.model.User;
import flights.service.LoginService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class LoginServlet extends HttpServlet {
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String userId = request.getParameter("username");
		String password = request.getParameter("password");
		LoginService loginService = new LoginService();
		boolean result = loginService.authenticateUser(userId, password);
		User user = loginService.getUserByUserName(userId);

		if(result == true){
			request.getSession().setAttribute("user", user);
			if(user.getRole().equals("admin")) {
                response.sendRedirect("flights.jsp");
            }
			else
				if(user.getRole().equals("user"))
					response.sendRedirect("http://localhost:8080/flights/list");
		}
		else{
			response.sendRedirect("error.jsp");
		}


	}

}