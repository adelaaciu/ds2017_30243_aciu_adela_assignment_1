package flights.controller;


import flights.model.Flight;
import flights.service.CityService;
import flights.service.FlightService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;

public class TimeController extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        long flightId = Long.valueOf(request.getPathInfo().replace("/", ""));
        FlightService service = FlightService.getInstance();
        Flight flight = service.getFlight(String.valueOf(flightId));

        CityService cityService = CityService.getInstance();
        Timestamp arrivalTime = cityService.getLocalTime(flight.getArrivalCity(), flight.getArrivalTime());
        Timestamp departureTime = cityService.getLocalTime(flight.getArrivalCity(), flight.getDepartureTime());


        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";

        out.println(docType +
                "<html>\n" +
                "<head><title>" + "Local time" + "</title></head>\n" +
                "<body bgcolor = \"#f0f0f0\">\n" +

                "<h1 align = \"center\">" + "DB TIME" + "</h1>\n" +
                "<div>" + flight.getArrivalTime() + "</div>" +
                "<div>" + flight.getDepartureTime() + "</div>" +
                "<h1 align = \"center\">" + "Local time" + "</h1>\n" +
                "<div>" + arrivalTime + "</div>" +
                "<div>" + departureTime + "</div>" +
                "</body> " +
                "</html>"
        );
    }

}
