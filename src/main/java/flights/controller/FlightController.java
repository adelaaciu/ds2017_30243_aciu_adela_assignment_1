package flights.controller;

import flights.model.Flight;
import flights.service.FlightService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class FlightController extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getServletPath();
        flightList(request, response);
    }

    private void flightList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        FlightService service = FlightService.getInstance();
        List<Flight> flightList = service.getFlights();

        RequestDispatcher dispatcher = request.getRequestDispatcher("flight-list.jsp");
        createResponse(flightList, response);
    }

    private void createResponse(List<Flight> flightList, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String docType =
                "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
        StringBuilder stringBuffer = new StringBuilder();

        for (Flight f : flightList)
            stringBuffer
                    .append("<tr>" + " <td>")
                        .append(f.getId())
                    .append("</td>")
                    .append(" <td>")
                        .append(f.getType())
                    .append(" </td>")
                    .append(" <td>")
                        .append(f.getArrivalCity().getName())
                    .append(" </td>")
                    .append(" <td>")
                        .append(f.getDepartureCity().getName())
                    .append("</td>")
                    .append("<td>")
                        .append("<a href=/time/" + f.getId() + ">Time").append("</a>")
                    .append("</td>")
                    .append("</tr>");
        out.println(docType +
                "  <table border=\"1\" cellpadding=\"10\">" +
                " <tr> " +
                " <th>ID</th>" +
                " <th>Type</th>" +
                " <th>Arrival City</th>" +
                " <th>Departure City</th>" +
                " <th>Get Local Date</th>" +
                "</tr>" + stringBuffer + "</table>");
    }

}
