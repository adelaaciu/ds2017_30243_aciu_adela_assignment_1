package flights.util;

import flights.model.Flight;
import flights.model.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

public class Hibernate {

    private static final SessionFactory sessionFactory;

    static {
        try {
//            Configuration cfg = new Configuration();
//            cfg.addClass(User.class);
//            cfg.addClass(Flight.class);
//            cfg.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
//            cfg.setProperty("hibernate.connection.datasource", "jdbc:mysql://localhost:3306/flights");
//            cfg.
//            Configuration cfg = new Configuration().configure("hibernate.cfg.xml").addAnnotatedClass(User.class).addAnnotatedClass(Flight.class);
            sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session openSession() {
        return sessionFactory.openSession();
    }
}