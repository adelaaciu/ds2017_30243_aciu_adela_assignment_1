package flights.dao;


import flights.model.City;
import flights.model.Flight;
import flights.util.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FlightDAO {
    private static FlightDAO flightDAO;

    private FlightDAO() {
    }

    public static FlightDAO getInstance() {
        if (flightDAO == null) {
            flightDAO = new FlightDAO();
        }
        return flightDAO;
    }

    public String addFlight(String type, String departureCityName, String arrivalCityName, Date departureTime, Date arrivalTime) {
        Transaction tx = null;
        CityDAO cityDAO = new CityDAO();
        City arrivalCity = cityDAO.getCity(arrivalCityName);
        City departureCity = cityDAO.getCity(departureCityName);

        Flight flight = new Flight(type, arrivalCity, departureCity, departureTime, arrivalTime);
        try (Session session = Hibernate.openSession()) {
            tx = session.getTransaction();
            tx.begin();

            long flightId = (long) session.save(flight);
            flight.setId(flightId);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }

        return "CREATED";
    }

    public String deleteFlight(int number) {
        Transaction tx = null;
        Flight flightDao = null;
        try (Session session = Hibernate.openSession()) {
            tx = session.getTransaction();
            tx.begin();
            flightDao = getFlight(String.valueOf(number));
            session.delete(flightDao);

            if (flightDao != null) {
                session.delete(flightDao);
            }

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
            return "Not_found";
        }
        return "Deleted";
    }

    public String updateFlight(long id, String type, String departureCityName, String arrivalCityName, Date departureTime, Date arrivalTime) {
        Transaction tx = null;
        Flight flightDao = null;
        CityDAO cityDAO = new CityDAO();
        try (Session session = Hibernate.openSession()) {
            tx = session.getTransaction();
            tx.begin();
            flightDao = getFlight(String.valueOf(id));
            if (flightDao != null) {
                if (type != "") {
                    flightDao.setType(type);
                }

                if (arrivalCityName != "") {
                    City arrivalCity = cityDAO.getCity(arrivalCityName);
                    flightDao.setDepartureCity(arrivalCity);
                }
                if (departureCityName != "") {
                    City departureCity = cityDAO.getCity(departureCityName);
                    flightDao.setDepartureCity(departureCity);
                }

                if (departureTime != null) {
                    flightDao.setDepartureTime(departureTime);
                }
                if (arrivalTime != null) {
                    flightDao.setArrivalTime(arrivalTime);
                }
            }
            session.update(flightDao);
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }

        if (flightDao == null)
            return "NOT_FOUND";

        return "UPDATED";
    }

    public Flight getFlight(String num) {
        Session session = Hibernate.openSession();
        Transaction tx = null;
        Flight flightDao = null;
        try {
            tx = session.getTransaction();
            tx.begin();
            String sql = "from Flight where id=" + num;
            Query query = session.createQuery(sql);
            flightDao = (Flight) query.uniqueResult();

            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return flightDao;
    }


    public List getFlights() {
        List list = new ArrayList<>();
        Transaction tx = null;
        Flight flightDao = null;
        try (Session session = Hibernate.openSession()) {
            tx = session.getTransaction();
            tx.begin();
            list = session.createQuery("from Flight ").list();

            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        }
        return list;
    }
}
