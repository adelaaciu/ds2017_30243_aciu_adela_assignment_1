package flights.dao;

import flights.model.City;
import flights.util.Hibernate;
import org.hibernate.*;
import org.hibernate.query.Query;

public class CityDAO {


    public City addCity(City city){
        Session session = Hibernate.openSession();
        Transaction tx = null;

        try {
            tx = session.getTransaction();
            tx.begin();

            City cityDao  = getCity(city.getName());
            if(cityDao == null) {
                city.setId((Long) session.save(city));
            }
            tx.commit();
        } catch (HibernateException e) {
            if (tx!=null) tx.rollback();
            e.printStackTrace();
        } finally {
            session.close();
        }
        return city;
    }

    public City getCity(String name){
        Transaction tx = null;
        City city= null;
        try (Session session = Hibernate.openSession()) {
            tx = session.getTransaction();
            tx.begin();

            String sql = "from City where name='" + name + "'";
            Query query = session.createQuery(sql);
            city = (City) query.uniqueResult();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
        return city;
    }

    public City getCity(long id){
        Transaction tx = null;
        City city= null;
        try (Session session = Hibernate.openSession()) {
            tx = session.getTransaction();
            tx.begin();

            String sql = "from City where id=" + id + "";
            Query query = session.createQuery(sql);
            city = (City) query.uniqueResult();

            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) tx.rollback();
            e.printStackTrace();
        }
        return city;
    }
}
