package flights.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="city")
public class City implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    @Column(name = "name")
    private String name;
    @Column(name = "latitude")
    private String latitude;
    @Column(name = "longitude")
    private String longitude;

    public City() {
    }

    public City(long id, String name, String latitude, String longitude) {
        this.id = id;
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

//    public List<Flight> getArrivalFlight() {
//        return arrivalFlight;
//    }
//
//    public void setArrivalFlight(List<Flight> arrivalFlight) {
//        this.arrivalFlight = arrivalFlight;
//    }
//
//    public List<Flight> getDepartureFlight() {
//        return departureFlight;
//    }
//
//    public void setDepartureFlight(List<Flight> departureFlight) {
//        this.departureFlight = departureFlight;
//    }
}
