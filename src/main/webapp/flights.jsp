<%@ page import="flights.model.User" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%
    String role = null;
    try {
        role = ((User) session.getAttribute("user")).getRole();
    }
    catch (Exception ex) {
        response.sendRedirect("error.jsp");
    }
    if(!"admin".equals(role)){
        response.sendRedirect("error.jsp");
    }
    else{
%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
    <title>AdminOperations</title>
</head>
<body>

<form method="GET" action="flights/get">
    <div>
        Flight numeber:
        <input type="text" name="number"><br/>
        <button type="submit">FindFlight</button>
    </div>
</form>


<form method="post" action="flights/post">
    Airplane:
    <input type="text" name="airplaneType"><br/>
    Departure City:
    <select name="departureCityPost">
        <option value="Cluj-Napoca">Cluj-Napoca</option>
        <option value="Dubai">Dubai</option>
        <option value="NewYork">NewYork</option>
    </select>
    Departure Date:
    <input type="datetime-local" name="departureDatePost"><br/>

    Arrival City Name:
    <select name="arrivalCityPost">
        <option value="Cluj-Napoca">Cluj-Napoca</option>
        <option value="Dubai">Dubai</option>
        <option value="NewYork">NewYork</option>
    </select>

    Arrival Date:
    <input type="datetime-local" name="arrivalDatePost"><br/>
    <input type="submit" value="Create"/>
</form>

<form method="post" action="flights/put">
    Flight numeber:
    <input type="text" name="flightNumber"><br/>
    Airplane:
    <input type="text" name="airplaneType"><br/>
    Departure City:
    <select name="departureCityUpdate">
        <option value="Cluj-Napoca">Cluj-Napoca</option>
        <option value="Dubai">Dubai</option>
        <option value="NewYork">NewYork</option>
    </select>
    Departure Date:
    <input type="datetime-local" name="departureDate"><br/>

    Arrival City Name:
    <select name="arrivalCityUpdate">
        <option value="Cluj-Napoca">Cluj-Napoca</option>
        <option value="Dubai">Dubai</option>
        <option value="NewYork">NewYork</option>
    </select>

    Arrival Date:
    <input type="datetime-local" name="arrivalDate"><br/>
    <input type="submit" value="Create"/>
</form>


<form method="get" action="/flights/delete">
    Flight numeber:
    <input type="text" name="flightNumber"><br/>
    <input type="submit" value="DeleteFlight"/>
</form>

<div>
    <a href="${pageContext.request.contextPath}/flights/list">Check all Flights</a>
</div>
</body>
</html>
<%
}
%>
