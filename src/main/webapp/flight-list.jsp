<%--
  Created by IntelliJ IDEA.
  User: adela
  Date: 10/30/2017
  Time: 10:48 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>User page</title>
</head>
<body>
<caption><h2>List of Flights</h2></caption>
<table>
    <tr>
        <th>ID</th>
        <th>Type</th>
        <th>Arrival City</th>
        <th>Departure City</th>
        <th>Get Local Time</th>
    </tr>
    <%--@elvariable id="flightList" type="java.util.List"--%>
    <c:forEach var="flight" items="${flightList}">
        <tr>
            <td><c:out value="${flight.id}"/></td>
            <td><c:out value="${flight.type}"/></td>
            <td><c:out value="${flight.arrivalCity.name}"/></td>
            <td><c:out value="${flight.departureCity.name}"/></td>
            <td>
                <a href="${pageContext.request.contextPath}/edit?id=<c:out value='${flight.id}' />">GetLocalDate</a>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
